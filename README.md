# Ansible Role: Kubescape

[![pipeline status](https://gitlab.com/enmanuelmoreira/ansible-role-kubescape/badges/main/pipeline.svg)](https://gitlab.com/enmanuelmoreira/ansible-role-kubescape/-/commits/main)

This role installs [Kubescape](https://github.com/armosec/kubescape) binary on any supported host.

## Requirements

github3.py >= 1.0.0a3

It can be installed from pip:

```bash
pip install github3.py
```

## Role Variables

Available variables are listed below, along with default values (see `defaults/main.yml`):

    kubescape_version: latest # v0.7.11 if you want a specific version
    setup_dir: /tmp
    kubescape_bin_path: /usr/local/bin/kubescape
    kubescape_repo_path: https://github.com/gruntwork-io/kubescape/releases/download

This role can install the latest or a specific version. See [available Kubescape releases](https://github.com/armosec/kubescape) and change this variable accordingly.

    kubescape_version: latest # v0.7.11 if you want a specific version

The path of the Kubescape repository.

    kubescape_repo_path: https://github.com/gruntwork-io/kubescape/releases/download

The location where the Kubescape binary will be installed.

    kubescape_bin_path: /usr/local/bin/kubescape

Kubescape needs a GitHub token so it can be downloaded. You must use the **github_token** variable in `vars/main.yml` in order to install Kubescape. However, this value can be empty until GitHub reaches the maximum number of anonymous connections.

## Dependencies

None.

## Example Playbook

    - hosts: all
      become: yes
      roles:
        - role: kubescape

## License

MIT / BSD
